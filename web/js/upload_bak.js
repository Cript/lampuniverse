$(function () {
    'use strict';

    var showMessage = function(message) {
        alert(message);
    };

    var FileModel = Backbone.Model.extend({
        defaults: function () {
            return {
                originalName: null,
                link: '#',
                parseLink: '#',
                loadTime: null,
                supplier: null,
                date: null,
                number: null,
                total: null,
                parsed: false,
                processed: false,
                income: false
            }
        }
    });

    var ProductModel = Backbone.Model.extend({
        initialize: function() {
            this.set('totalCalculated', this.get('price') * this.get('number'));
        },
        defaults: function () {
            return {
                article: null,
                articleFind: null,
                price: null,
                number: null,
                totalCalculated: null,
                total: null,
                GTDNumber: null
            }
        }
    });

    var FileCollection = Backbone.Collection.extend({
        model: FileModel,
        url: 'invoice'
    });

    var FileParseCollection = Backbone.Collection.extend({
        model: FileModel,
        url: 'invoice_parse'
    });

    var ProductCollection = Backbone.Collection.extend({
        model: ProductModel,
        url: 'product',
        saveAll: function() {
            var requestDeferred = [];
            this.each(function(product) {
                requestDeferred.push(Backbone.sync.call(this, 'update', product));
            });
            return $.when.apply(null, requestDeferred);
        },
        existsWithoutArticle: function() {
            var products = this.find(function(product) {
                return product.get("articleFind") === false;
            });
            return (products) ? true : false;
        }
    });

    var fileCollection = new FileCollection();

    var fileParseCollection = new FileParseCollection();

    var productCollection = new ProductCollection();

    var FileView = Backbone.View.extend({
        tagName: 'tr',
        events: {
            'click .parse': 'onParse',
            'click .delete': 'onDelete'
        },
        initialize: function() {
            this.listenTo(this.model, 'destroy', this.remove);
        },
        render: function() {
            this.$el.html(ich.template_pdf(this.model.toJSON()));
            appView.generateSelect(this.$el.find('select'));
            return this;
        },
        onParse: function (event) {
            var that = this;
            var supplierId = this.checkSupplierValue();
            if(supplierId == 0) {
                alert('Необходимо выбрать поставщика');
            } else {
                $(event.currentTarget).button('loading');
                $.getJSON('invoice/parse/' + this.model.id, {supplierId: supplierId}).done(function (data) {
                    that.model.set('supplier', data.supplier);
                    that.model.set('parseLink', data.parseLink);
                    that.model.set('parseTime', data.parseTime);
                    that.model.set('parsed', data.parsed);
                    that.model.set('processed', data.processed);
                    fileCollection.remove(that.model);
                    that.$el.remove();
                    fileParseCollection.add(that.model);
                });
            }
        },
        onDelete: function() {
            if(confirm('Удалить эту запись?')) {
                this.model.destroy({
                    wait: true
                });
            }
        },
        checkSupplierValue: function() {
            return this.$el.find('select option:selected').val();
        }
    });

    var FileParseView = Backbone.View.extend({
        tagName: 'tr',
        events: {
            'click .parse': 'onParse',
            'click .delete': 'onDelete',
            'click .result': 'onResult'
        },
        initialize: function() {
            this.listenTo(this.model, 'destroy', this.remove);
            this.listenTo(this.model, 'remove', this.remove);
        },
        render: function() {
            this.$el.html(ich.template_parse_file(this.model.toJSON()));
            appView.generateSelect(this.$el.find('select'), this.model.get('supplier'));
            return this;
        },
        onParse: function(event) {
            var that = this;
            $(event.currentTarget).button('loading');
            $.getJSON('invoice/parse_data/' + this.model.id).done(function (data) {
                that.model.set('date', data.date);
                that.model.set('number', data.number);
                that.model.set('total', data.total);
                that.model.set('parsed', data.parsed);
                that.model.set('processed', data.processed);
                that.render();
            });
        },
        onResult: function() {
            modalProductsView = new ModalView({
                model: this.model
            });
        },
        onDelete: function() {
            if(confirm('Удалить эту запись?')) {
                this.model.destroy({
                    wait: true
                });
            }
        }
    });

    var ProductView = Backbone.View.extend({
        tagName: 'tr',
        initialize: function() {
            this.listenTo(this.model, 'destroy', this.remove);
            this.listenTo(this.model, 'change:article', this.onChangeArticle);
        },
        events: {
            'click .article-find': 'onArticleFind',
            'keyup input.price': 'onChangePrice',
            'keyup input.number': 'onChangeNumber',
            'keyup input.total-from-file': 'onChangeTotalFromFile'
        },
        render: function() {
            //this.$el.data('view_id', this.model.get('id'));
            this.$el.html(ich.template_product(this.model.toJSON()));
            return this;
        },
        onArticleFind: function() {
            new ModalSearchView({
                model: this.model
            });
        },
        onChangeArticle: function() {
            this.$('input.article').val(this.model.get('article'));
        },
        onChangePrice: function(event) {
            var summ = event.currentTarget.value * this.$('input.number').val();
            this.$('.product-total-calculated').text(summ);
            modalProductsView.trigger('productTotalPriceChange');
        },
        onChangeNumber: function(event) {
            var summ = event.currentTarget.value * this.$('input.price').val();
            this.$('.product-total-calculated').text(summ);
            modalProductsView.trigger('productTotalPriceChange');
        },
        onChangeTotalFromFile: function() {
            modalProductsView.trigger('productTotalFromFilePriceChange');
        }
    });

    var ModalView = Backbone.View.extend({
        tagName: 'div',
        className: 'modal fade products-modal',
        events: {
            'click .cancel': 'onCancel',
            'click .save': 'onSave',
            'click .save-and-complete': 'onSaveAndComplete'
        },
        productsViews: [],
        $productList: null, //Элемент таблицы товаров
        initialize: function() {

            this.listenTo(productCollection, 'add', this.addProduct);
            this.listenTo(this, 'productTotalPriceChange', this.countTotalCalculated);
            this.listenTo(this, 'productTotalFromFilePriceChange', this.countTotalFromFile);
            this.render();

            var that = this;
            this.$el.on('hidden.bs.modal', function() {
                that.remove();
            });

            productCollection.reset();
            productCollection.fetch({
                data: {
                    invoice: this.model.get('id')
                }
            }).done(function() {
                that.countTotalFromFile();
                that.countTotalCalculated();
                that.$el.modal();
            });

            this.$productList = this.$el.find('.product-list');
        },
        render: function() {
            this.$el.html(ich.modal(this.model.toJSON()));
            return this;
        },
        addProduct: function(product) {
            var view = new ProductView({model: product});
            this.productsViews.push(view);
            this.$productList.find('tr:last').before(view.render().el)
        },
        countTotalCalculated: function() {
            /*Подсчет суммы товаров по значениям из файла. Заполняется колонка "Сумма расчетная" */
            var sum = _.reduce(this.$productList.find('td.product-total-calculated'), function(memo, value) {
                return memo + parseFloat($(value).text());
            }, 0);
            sum = Math.floor(sum*100)/100;
            this.$productList.find('tr:last td.total-calculated p.calculated span').text(sum);
            this.countDifference();
            //this.$productList.find('tr:last td.difference p.difference span').text(Math.floor((this.model.get('total') - sum)*100)/100);
        },
        countTotalFromFile: function()
        {
            var sum = _.reduce(this.$productList.find('input.total-from-file'), function(memo, value) {
                return memo + parseFloat($(value).val());
            }, 0);
            sum = sum.toFixed(2);
            this.$productList.find('tr:last td.total-from-file p.calculated span').text(sum);
        },
        countDifference: function() {
            var sum = this.$productList.find('tr:last td.total-from-file p.calculated span').text() -
                this.$productList.find('tr:last td.total-calculated p.calculated span').text();
            sum = sum.toFixed(2);
            this.$productList.find('tr:last td.difference p.difference span').text(sum);
        },
        onCancel: function() {
            if(confirm('Закрыть окно?')) {
                this.$el.modal('hide');
            }
        },
        onSave: function(event) {
            if(confirm('Сохранить изменения?')) {
                var that = this;
                $(event.currentTarget).button('loading');

                this.updateProductModels();
                productCollection.saveAll().done(function() {
                    $.when(that.model.save({
                        number: that.$('.modal-title input.number').val(),
                        date: that.$('.modal-title input.date').val(),
                        total: that.$productList.find('.total-from-file input.total-from-file-file').val()
                    }, {wait: true})).done(function() {
                        that.$el.modal('hide');
                    });
                }).fail(function() {
                    $(event.currentTarget).button('reset');
                    console.log(arguments);
                    alert('Произошла ошибка!');
                });
            }
        },
        onSaveAndComplete: function(event) {
            if(true == productCollection.existsWithoutArticle()) {
                showMessage('Есть товары с неопределенными артукулами!');
                return false;
            }
            if(confirm('Сохранить изменения и оприходовать?')) {
                var that = this;
                $(event.currentTarget).button('loading');

                this.updateProductModels();
                productCollection.saveAll().done(function() {
                    $.when(that.model.save({
                        number: that.$('.modal-title input.number').val(),
                        date: that.$('.modal-title input.date').val(),
                        total: that.$productList.find('.total-from-file input.total-from-file-file').val(),
                        updateStatus: true
                    }, {wait: true})).done(function() {
                        console.log(that.model);
                        fileParseCollection.remove(that.model);
                        that.$el.modal('hide');
                    });
                }).fail(function() {
                    $(event.currentTarget).button('reset');
                    console.log(arguments);
                    alert('Произошла ошибка!');
                });
            }
        },
        updateProductModels: function() {
            _.each(this.productsViews, function(view) {
                view.model.set({
                    article: view.$('input.article').val(),
                    price: view.$('input.price').val(),
                    number: view.$('input.number').val(),
                    countryName: view.$('input.country-name').val(),
                    GTDNumber: view.$('input.gtd-number').val()
                })
            });
        }
    });

    var ModalSearchView = Backbone.View.extend({
        tagName: 'div',
        className: 'modal fade article-find-modal',
        $articleList: null,
        initialize: function() {
            this.render();
            this.$el.modal();
            this.$articleList = this.$('.article-list');
            this.setArticles();
        },
        events: {
            'click .search': 'onSearch',
            'click .insert_article': 'onInsertArticle',
            'click .articles a': 'onSelectArticle'
        },
        render: function() {
            this.$el.html(ich.modal_search(this.model.toJSON()));
            return this;
        },
        setArticles: function() {
            var that = this;
            var articles = this.model.get('article').replace(/\s+/g, '').split(',');
            articles = _.map(articles, function(article) {
                return '<small><a href="#">'+article+'</a></small>';
                //;
            });
            that.$('.articles').html(articles.join(', '));
        },
        onSelectArticle: function(event)
        {
            this.$('.by-article').val($(event.currentTarget).text())
        },
        onSearch: function(event) {
            event.preventDefault();
            var q = this.$('.by-article').val();
            var that = this;
            if(q) {
                that.$articleList.find('tr:gt(0)').remove();
                $.getJSON('product_entity', {q: q}).done(function (data) {
                    if(data.length == 0) {
                        that.$articleList.append('<tr><td>Ничего не найдено</td></tr>')
                    } else {
                        _.each(data, function (value) {
                            that.$articleList.append('<tr><td><a href="#" class="insert_article">' + value.sku + '</a></td></tr>')
                        })
                    }
                });
            }
        },
        onInsertArticle: function(event) {
            event.preventDefault();
            this.model.set('article', $(event.currentTarget).text());
            this.model.set('articleFind', true);
            this.$el.modal('hide');
        }
    });

    var AppView = Backbone.View.extend({
        el: $('.file-list'),
        suppliers: [],
        initialize: function() {
            this.$filesList = this.$el.find('.pdf-list');
            this.$filesParseList = this.$el.find('.parse-list');
            this.$filesIncomeList = this.$('.income-list');
            this.listenTo(fileCollection, 'add', this.addFile);
            this.listenTo(fileParseCollection, 'add', this.addParseFile);
            var that = this;

            /* Запрос поставщиков и счет-фактур */
            this.suppliersRequestDeferred = $.Deferred();
            this.getSuppliers();
            this.suppliersRequestDeferred.done(function() {
                fileCollection.fetch({
                    success: function(collection) {
                        if(collection.length == 0) {
                            that.hideTable(that.$filesList);
                        }
                    }
                });
                fileParseCollection.fetch({
                    success: function(collection) {
                        if(collection.length == 0) {
                            that.hideTable(that.$filesParseList);
                        }
                    }
                });
            });
        },
        addFile: function(file) {
            var view = new FileView({model: file});
            this.showTable(this.$filesList);
            var viewElement = view.render().el;
            //this.generateSelect($(viewElement).find('select'));
            this.$filesList.append(viewElement);
        },
        addParseFile: function(file) {
            var view = new FileParseView({model: file});
            this.showTable(this.$filesParseList);

            var viewElement = view.render().el;
            //this.generateSelect($(viewElement).find('select'), file.get('supplier'));
            this.$filesParseList.append(viewElement);
        },
        showTable: function(element) {
            element.show().siblings('.alert').hide();
        },
        hideTable: function(element) {
            element.hide().siblings('.alert').show();
        },
        getSuppliers: function() {
            $.getJSON('/suppliers').done(function(data) {
                appView.suppliers = data;
                appView.suppliersRequestDeferred.resolve();
            }).fail(function(data) {
                alert('Ошибка запроса поставщиков: '+data.statusText);
            });
        },
        generateSelect: function(element, supplierId) {
            var supplierId = supplierId || null;
            $.each(this.suppliers, function(key, value) {
                $(element)
                    .append($("<option></option>")
                        .attr("value", value.id)
                        .text(value.name));
            });
            if(supplierId) {
                $(element).val(supplierId);
            }
        }
    });

    var appView = new AppView();

    var modalProductsView;

    $('form[name=invoice]').fileupload({
        dataType: 'json',
        autoUpload: true
    }).on('fileuploaddone', function (e, data) {
        //_.each(data.files, function (file, key) {
            if(data.result.error == undefined) {
                fileCollection.add({
                    id: data.result.id,
                    originalName: data.result.originalName,
                    link: data.result.link,
                    loadTime: data.result.loadTime
                });
            } else {
                alert('Ошибка при загрузке файла! ' + data.result.error);
            }
        //});
    })
});