require.config({
    paths: {
        jquery: 'lib/jquery',
        underscore: 'lib/underscore',
        backbone: 'lib/backbone',
        bootstrap: 'lib/bootstrap',
        ich: 'lib/ICanHaz.min',
        'jquery.ui.widget': 'lib/jQueryFileUpload/jquery.ui.widget'
    },
    shim: {
        'ich': {
            exports: 'ich'
        },
        'bootstrap': {
            deps: ['jquery'],
            exports: 'bootstrap'
        }
    }
});

define([
    'jquery',
    'app/pageUpload'
], function($, pageUpload) {
    //$(document).load(function () {
        pageUpload.initialize();
    //});
});
