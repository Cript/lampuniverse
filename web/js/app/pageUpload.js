define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
    'app/supplier/collection',
    'app/upload/views/list',
    'app/parse/views/list',
    'lib/jQueryFileUpload/jquery.fileupload'
], function($, _, Backbone, Bootstrap, supplierCollection, UploadView, ParseView) {

    var initialize = function() {
        var uploadView;
        var parseView;
        supplierCollection.fetch().done(function() {
            uploadView = new UploadView();
            parseView = new ParseView();
        });

        $('form[name=invoice]').fileupload({
            dataType: 'json',
            autoUpload: true
        }).on('fileuploaddone', function (e, data) {
            if(data.result.error == undefined) {
                uploadView.collection.add({
                    id: data.result.id,
                    originalName: data.result.originalName,
                    link: data.result.link,
                    loadTime: data.result.loadTime
                });
            } else {
                alert('Ошибка при загрузке файла! ' + data.result.error);
            }
        });
    };

    return {
        initialize: initialize
    };

});