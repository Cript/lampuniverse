define([
    'jquery',
    'underscore',
    'backbone',
    'app/parse/collection',
    'app/parse/views/item'
], function($, _, Backbone, parseCollection, ParseViewItem) {
    var ParseView = Backbone.View.extend({
        el: $('.parse-list'),
        initialize: function() {
            this.collection = parseCollection;
            this.listenTo(this.collection, 'add', this.addFile);
            this.collection.fetch();
        },
        addFile: function(model) {
            var view = new ParseViewItem({model: model});
            var viewElement = view.render().el;
            this.$el.append(viewElement);
        }
    });

    return ParseView;
});