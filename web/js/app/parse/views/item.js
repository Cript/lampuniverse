define([
    'jquery',
    'backbone',
    'ich',
    'app/supplier/collection',
    'app/product/views/modal'
], function($, Backbone, ich, supplierCollection, ModalView) {
    var ViewItem = Backbone.View.extend({
        tagName: 'tr',
        events: {
            'click .parse': 'onParse',
            'click .delete': 'onDelete',
            'click .result': 'onResult'
        },
        initialize: function() {
            this.listenTo(this.model, 'destroy', this.remove);
            this.listenTo(this.model, 'remove', this.remove);
        },
        render: function() {
            //console.log(ich.template_parse_file);
            this.$el.html(ich.template_parse_file(this.model.toJSON()));
            supplierCollection.generateSelect(this.$el.find('select'), this.model.get('supplier'));
            return this;
        },
        onParse: function(event) {
            var that = this;
            $(event.currentTarget).button('loading');
            $.getJSON('invoice/parse_data/' + this.model.id).done(function (data) {
                that.model.set('date', data.date);
                that.model.set('number', data.number);
                that.model.set('total', data.total);
                that.model.set('totalTax', data.totalTax);
                that.model.set('parsed', data.parsed);
                that.model.set('processed', data.processed);
                that.render();
            }).error(function() {
                $(event.currentTarget).button('reset');
                alert('Ошибка при парсинге файла!');
            });
        },
        onResult: function() {
            new ModalView({
                model: this.model
            });
        },
        onDelete: function() {
            if(confirm('Удалить эту запись?')) {
                this.model.destroy({
                    wait: true
                });
            }
        }
    });

    return ViewItem;
});