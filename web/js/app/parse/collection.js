define([
    'jquery',
    'underscore',
    'backbone',
    'app/upload/model'
], function($, _, Backbone, ParseModel) {
    var ParseCollection = Backbone.Collection.extend({
        model: ParseModel,
        url: 'invoice_parse'
    });

    var parseCollection = new ParseCollection();

    return parseCollection;
});