define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap',
    'app/supplier/collection',
    'app/income/views/list',
    'app/income/views/filter'
], function($, _, Backbone, Bootstrap, supplierCollection, IncomeListView, FilterView) {

    var initialize = function() {
        supplierCollection.fetch().done(function () {
            new IncomeListView();
            new FilterView();
        });
    };

    return {
        initialize: initialize
    };
});