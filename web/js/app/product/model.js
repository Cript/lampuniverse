define([
    'jquery',
    'underscore',
    'backbone'
], function($, _, Backbone) {
    var ProductModel = Backbone.Model.extend({
        initialize: function() {
            var totalCalculated = this.get('price') * this.get('number');
            this.set('totalCalculated', totalCalculated.toFixed(2));
        },
        defaults: function () {
            return {
                article: null,
                articleFind: false,
                price: null,
                number: null,
                totalCalculated: null,
                total: null,
                totalTax: null,
                countryCode: null,
                countryName: null,
                GTDNumber: null,
                invoiceId: null,
                manualAdd: false
            }
        }
    });
    return ProductModel;
});


