define([
    'jquery',
    'backbone',
    'ich',
    'app/product/views/modalSearch'
], function($, Backbone, ich, modalSearchView) {
    var ProductView = Backbone.View.extend({
        tagName: 'tr',
        className: 'product',
        initialize: function() {
            this.listenTo(this.model, 'destroy', this.remove);
            this.listenTo(this.model, 'change:article', this.onChangeArticle);
        },
        events: {
            'click .article-find': 'onArticleFind',
            'click .save a': 'onSave',
            'click .remove a': 'onRemove',
            'keyup input.price': 'onChangePrice',
            'keyup input.number': 'onChangeNumber',
            'keyup .total-from-file input.file': 'onChangeTotalFromFile',
            'keyup .total-tax-from-file input.file': 'onChangeTotalTaxFromFile'
        },
        render: function() {
            this.$el.html(ich.template_product(this.model.toJSON()));
            return this;
        },
        onArticleFind: function() {
            new modalSearchView({
                model: this.model
            });
        },
        onSave: function() {
            if(confirm('Добавить товар?')) {
                this.model.save({
                    article: this.$el.find('input.article').val(),
                    price: this.$el.find('.price input').val(),
                    number: this.$el.find('.number input').val(),
                    total: this.$el.find('.total-from-file input').val(),
                    totalTax: this.$el.find('.total-tax-from-file input').val(),
                    countryName: this.$el.find('.country-name input').val(),
                    GTDNumber: this.$el.find('.gtd-number input').val(),
                    manualAdd: false,
                    wait: true
                }).done(this.render());
            }
        },
        onRemove: function() {
            if(confirm('Удалить товар?')) {
                //this.onChangeTotalFromFile();
                //this.onChangeTotalTaxFromFile();
                this.model.destroy({
                    wait: true
                });
            }
        },
        onChangeArticle: function() {
            this.$('input.article').val(this.model.get('article'));
            this.$('input.article').parents('.has-error').removeClass('has-error').addClass('has-success');
        },
        onChangePrice: function(event) {
            var summ = event.currentTarget.value * this.$('input.number').val();
            this.$('.product-total-calculated').text(summ);
            this.model.trigger('priceChange');
        },
        onChangeNumber: function(event) {
            var summ = event.currentTarget.value * this.$('input.price').val();
            this.$('.product-total-calculated').text(summ);
            this.model.trigger('priceChange');
        },
        onChangeTotalFromFile: function() {
            this.model.trigger('productTotalFromFilePriceChange');
        },
        onChangeTotalTaxFromFile: function() {
            this.model.trigger('productTotalTaxFromFilePriceChange');
        }
    });

    return ProductView;
});