define([
    'jquery',
    'underscore',
    'backbone',
    'app/product/collection',
    'app/product/views/item',
    'app/parse/collection'
], function($, _, Backbone, productCollection, ProductView, parseCollection) {
    var ModalView = Backbone.View.extend({
        tagName: 'div',
        className: 'modal fade products-modal',
        events: {
            'click .add-product': 'onAddProduct',
            'click .modal-footer .cancel': 'onCancel',
            'click .modal-footer .save': 'onSave',
            'click .modal-footer .save-and-complete': 'onSaveAndComplete'
        },
        productsViews: [],
        initialize: function() {

            this.collection = productCollection;

            this.listenTo(this.collection, 'add', this.addProduct);
            this.listenTo(this.collection, 'priceChange', this.countTotalCalculated);
            this.listenTo(this.collection, 'productTotalFromFilePriceChange', this.countTotalFromFile);
            this.listenTo(this.collection, 'productTotalTaxFromFilePriceChange', this.countTotalTaxFromFile);
            this.render();

            var that = this;
            this.$el.on('hidden.bs.modal', function() {
                that.remove();
            });

            productCollection.reset();
            productCollection.fetch({
                data: {
                    invoice: this.model.get('id')
                }
            }).done(function() {
                that.countTotalFromFile();
                that.countTotalTaxFromFile();
                that.countTotalCalculated();
                that.$el.modal();
            });

            this.$productList = this.$el.find('.product-list');
        },
        render: function() {
            this.$el.html(ich.modal(this.model.toJSON()));
            return this;
        },
        addProduct: function(product) {
            var view = new ProductView({model: product});
            this.productsViews.push(view);
            this.$productList.find('tr.total').before(view.render().el)
        },
        countTotalCalculated: function() {
            /*Подсчет суммы товаров по значениям из файла. Заполняется колонка "Сумма расчетная" */
            var sum = _.reduce(this.$productList.find('td.product-total-calculated'), function(memo, value) {
                return memo + parseFloat($(value).text());
            }, 0);
            sum = Math.floor(sum*100)/100;
            this.$productList.find('tr.total td.total-calculated p.calculated span').text(sum);
            this.countDifference();
        },
        countTotalFromFile: function()
        {
            var sum = _.reduce(this.$productList.find('tr.product td.total-from-file input.file'), function(memo, value) {
                return memo + parseFloat($(value).val());
            }, 0);
            sum = sum.toFixed(2);
            this.$productList.find('tr.total td.total-from-file p.calculated span').text(sum);
            this.countDifference();
        },
        countTotalTaxFromFile: function()
        {
            var sum = _.reduce(this.$productList.find('tr.product td.total-tax-from-file input.file'), function(memo, value) {
                return memo + parseFloat($(value).val());
            }, 0);
            sum = sum.toFixed(2);
            this.$productList.find('tr.total td.total-tax-from-file p.calculated span').text(sum);
        },
        countDifference: function() {
            var sum = this.$productList.find('tr.total td.total-from-file p.calculated span').text() -
                this.$productList.find('tr.total td.total-calculated p.calculated span').text();
            sum = sum.toFixed(2);
            this.$productList.find('tr.total td.total-from-file p.difference span').text(sum);
        },
        onAddProduct: function() {
            productCollection.add({
                invoiceId: this.model.get('id'),
                manualAdd: true
            });
        },
        onCancel: function() {
            if(confirm('Закрыть окно?')) {
                this.$el.modal('hide');
            }
        },
        onSave: function(event) {
            if(confirm('Сохранить изменения?')) {
                var that = this;
                $(event.currentTarget).button('loading');

                this.updateProductModels();
                productCollection.saveAll().done(function() {
                    $.when(that.model.save({
                        number: that.$('.modal-title input.number').val(),
                        date: that.$('.modal-title input.date').val(),
                        total: that.$productList.find('tr.total td.total-from-file input.file').val(),
                        totalTax: that.$productList.find('tr.total td.total-tax-from-file input.file').val()
                    }, {wait: true})).done(function() {
                        that.$el.modal('hide');
                    });
                }).fail(function() {
                    $(event.currentTarget).button('reset');
                    console.log(arguments);
                    alert('Произошла ошибка!');
                });
            }
        },
        onSaveAndComplete: function(event) {
            if(true == productCollection.existsWithoutArticle()) {
                if(!confirm('Есть товары с неопределенными артукулами! Все равно продолжить?')) {
                    return false;
                };
            }
            if(confirm('Сохранить изменения и оприходовать?')) {
                var that = this;
                $(event.currentTarget).button('loading');

                this.updateProductModels();
                productCollection.saveAll().done(function() {
                    $.when(that.model.save({
                        number: that.$('.modal-title input.number').val(),
                        date: that.$('.modal-title input.date').val(),
                        total: that.$productList.find('td.total-from-file input.file').val(),
                        updateStatus: true
                    }, {wait: true})).done(function() {
                        console.log(that.model);
                        parseCollection.remove(that.model);
                        that.$el.modal('hide');
                    });
                }).fail(function() {
                    $(event.currentTarget).button('reset');
                    console.log(arguments);
                    alert('Произошла ошибка!');
                });
            }
        },
        updateProductModels: function() {
            console.log(this.productsViews);
            _.each(this.productsViews, function(view) {
                console.log(view.$('input.price'));
                view.model.set({
                    article: view.$('input.article').val(),
                    price: view.$('input.price').val(),
                    number: view.$('input.number').val(),
                    total: view.$('td.total-from-file input').val(),
                    totalTax: view.$('td.total-tax-from-file input').val(),
                    countryName: view.$('input.country-name').val(),
                    GTDNumber: view.$('input.gtd-number').val()
                })
            });
        }
    });

    return ModalView;
});