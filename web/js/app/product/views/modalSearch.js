define([
    'jquery',
    'underscore',
    'backbone'
], function($, _, Backbone) {
    var ModalSearchView = Backbone.View.extend({
        tagName: 'div',
        className: 'modal fade article-find-modal',
        $articleList: null,
        initialize: function() {
            this.render();

            var that = this;
            this.$el.on('hidden.bs.modal', function() {
                that.remove();
                $('body').addClass('modal-open');
            });

            this.$el.modal();
            this.$articleList = this.$('.article-list');
            if(null != this.model.get('article')) {
                this.setArticles();
            }
        },
        events: {
            'click .search': 'onSearch',
            'click .insert_article': 'onInsertArticle',
            'click .articles a': 'onSelectArticle'
        },
        render: function() {
            this.$el.html(ich.modal_search(this.model.toJSON()));
            return this;
        },
        setArticles: function() {
            var that = this;
            var articles = this.model.get('article').replace(/\s+/g, '').split(',');
            articles = _.map(articles, function(article) {
                return '<small><a href="#">'+article+'</a></small>';
            });
            that.$('.articles').html(articles.join(', '));
        },
        onSelectArticle: function(event)
        {
            this.$('.by-article').val($(event.currentTarget).text())
        },
        onSearch: function(event) {
            event.preventDefault();
            var q = this.$('.by-article').val();
            var that = this;
            if(q) {
                that.$articleList.find('tr:gt(0)').remove();
                $.getJSON('product_entity', {q: q}).done(function (data) {
                    if(data.length == 0) {
                        that.$articleList.append('<tr><td>Ничего не найдено</td></tr>')
                    } else {
                        _.each(data, function (value) {
                            that.$articleList.append('<tr><td><a href="#" class="insert_article">' + value.sku + '</a></td></tr>')
                        })
                    }
                });
            }
        },
        onInsertArticle: function(event) {
            event.preventDefault();
            this.model.set('article', $(event.currentTarget).text());
            this.model.set('articleFind', true);
            this.$el.modal('hide');
        }
    });

    return ModalSearchView;
});