define([
    'jquery',
    'underscore',
    'backbone',
    'app/product/model'
], function($, _, Backbone, ProductModel) {
    var ProductCollection = Backbone.Collection.extend({
        model: ProductModel,
        url: 'product',
        saveAll: function() {
            var requestDeferred = [];
            this.each(function(product) {
                requestDeferred.push(Backbone.sync.call(this, 'update', product));
            });
            return $.when.apply(null, requestDeferred);
        },
        existsWithoutArticle: function() {
            var products = this.find(function(product) {
                return product.get("articleFind") === false;
            });
            return (products) ? true : false;
        }
    });

    var productCollection = new ProductCollection();

    return productCollection;
});