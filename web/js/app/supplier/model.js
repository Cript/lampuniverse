define([
    'underscore',
    'backbone'
], function(_, Backbone) {
    var SupplierModel = Backbone.Model.extend({
        defaults: function () {
            return {
                id: null,
                name: null
            }
        }
    });
    return SupplierModel;
});