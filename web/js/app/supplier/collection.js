define([
    'underscore',
    'backbone',
    'app/supplier/model'
], function(_, Backbone, SupplierModel) {
    var SuppliersCollection = Backbone.Collection.extend({
        model: SupplierModel,
        url: 'supplier',
        generateSelect: function(selectElement, supplierId) {
            var supplierId = supplierId || null;
            this.each(function(supplier) {
                $(selectElement)
                    .append($("<option></option>")
                        .attr("value", supplier.id)
                        .text(supplier.get('name')));
            });
            if(supplierId) {
                $(selectElement).val(supplierId);
            }
        },
        getSupplierById: function(id) {
            return this.find(function(supplier) {
                return supplier.id == id;
            });
        }
    });

    var supplierCollection = new SuppliersCollection();

    return supplierCollection;
});