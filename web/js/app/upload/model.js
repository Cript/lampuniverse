define([
    'jquery',
    'underscore',
    'backbone'
], function($, _, Backbone) {
    var FileModel = Backbone.Model.extend({
        defaults: function () {
            return {
                originalName: null,
                link: '#',
                parseLink: '#',
                loadTime: null,
                supplier: null,
                date: null,
                number: null,
                total: null,
                totalTax: null,
                parsed: false,
                processed: false,
                income: false
            }
        }
    });
    return FileModel;
});


