define([
    'jquery',
    'underscore',
    'backbone',
    'app/upload/model'
], function($, _, Backbone, UploadModel) {
    var UploadCollection = Backbone.Collection.extend({
        model: UploadModel,
        url: 'invoice'
    });

    var uploadCollection = new UploadCollection();

    return uploadCollection;
});