define([
    'jquery',
    'backbone',
    'ich',
    'app/supplier/collection',
    'app/upload/collection',
    'app/parse/collection'
], function($, Backbone, ich, supplierCollection, uploadCollection, parseCollection) {
    var ViewItem = Backbone.View.extend({
        tagName: 'tr',
        events: {
            'click .parse': 'onParse',
            'click .delete': 'onDelete'
        },
        initialize: function() {
            this.listenTo(this.model, 'destroy', this.remove);
        },
        render: function() {
            this.$el.html(ich.template_pdf(this.model.toJSON()));
            supplierCollection.generateSelect(this.$el.find('select'));
            return this;
        },
        onParse: function (event) {
            var that = this;
            var supplierId = this.checkSupplierValue();
            if(supplierId == 0) {
                alert('Необходимо выбрать поставщика');
            } else {
                $(event.currentTarget).button('loading');
                $.getJSON('invoice/parse/' + this.model.id, {supplierId: supplierId}).done(function (data) {
                    that.model.set('supplier', data.supplier);
                    that.model.set('parseLink', data.parseLink);
                    that.model.set('parseTime', data.parseTime);
                    that.model.set('parsed', data.parsed);
                    that.model.set('processed', data.processed);
                    uploadCollection.remove(that.model);
                    that.$el.remove();
                    parseCollection.add(that.model);
                });
            }
        },
        onDelete: function() {
            if(confirm('Удалить эту запись?')) {
                this.model.destroy({
                    wait: true
                });
            }
        },
        checkSupplierValue: function() {
            return this.$el.find('select option:selected').val();
        }
    });

    return ViewItem;
});