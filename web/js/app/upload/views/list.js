define([
    'jquery',
    'underscore',
    'backbone',
    'app/upload/collection',
    'app/upload/views/item'
], function($, _, Backbone, uploadCollection, UploadViewItem) {
    var UploadView = Backbone.View.extend({
        el: $('.upload-list'),
        initialize: function() {
            this.collection = uploadCollection;
            this.listenTo(this.collection, 'add', this.addFile);
            this.collection.fetch();
        },
        addFile: function(model) {
            var view = new UploadViewItem({model: model});
            var viewElement = view.render().el;
            //this.generateSelect($(viewElement).find('select'));
            this.$el.append(viewElement);
        }
    });

    return UploadView;
});