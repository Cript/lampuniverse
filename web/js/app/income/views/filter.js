define([
    'jquery',
    'underscore',
    'backbone',
    'bootstrap-datepicker-ru',
    'app/supplier/collection',
    'app/income/collection'
], function($, _, Backbone, datepicker, supplierCollection, incomeCollection) {

    var FilterView = Backbone.View.extend({
        el: $('.filter-view'),
        events: {
            'click .submit': 'onSubmit'
        },
        initialize: function() {
            this.$('.filter-daterange').datepicker({
                language: 'ru',
                autoclose: true,
                format: 'dd.mm.yyyy'
            });

            this.$supplierField = this.$('select.supplier');
            supplierCollection.generateSelect(this.$supplierField);
        },
        onSubmit: function(event) {
            event.preventDefault();
            incomeCollection.reset();
            incomeCollection.fetch({
                data: this.$('form').serializeArray()
            })
        }
    });

    return FilterView;
});