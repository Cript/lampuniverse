define([
    'jquery',
    'underscore',
    'backbone',
    'app/income/collection',
    'app/income/views/item'
], function($, _, Backbone, incomeCollection, IncomeItemView) {
    var IncomeListView = Backbone.View.extend({
        el: $('.income-list'),
        initialize: function() {
            this.listenTo(incomeCollection, 'add', this.addIncomeFile);
            this.listenTo(incomeCollection, 'reset', this.onCollectionReset);
            incomeCollection.fetch();
        },
        addIncomeFile: function(model) {
            var view = new IncomeItemView({model: model});
            var viewElement = view.render().el;
            this.$el.append(viewElement);
        },
        onCollectionReset: function() {
            this.$('tr:gt(0)').remove();
        }
    });

    return IncomeListView;
});