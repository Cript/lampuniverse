define([
    'jquery',
    'backbone',
    'ich',
    'app/supplier/collection',
    'app/product/views/modal'
], function($, Backbone, ich, supplierCollection, ModalView) {
    var ViewItem = Backbone.View.extend({
        tagName: 'tr',
        events: {
            'click .result': 'onResult',
            'click .delete': 'onDelete'
        },
        initialize: function() {
            this.listenTo(this.model, 'remove', this.remove);
        },
        render: function() {
            var supplier = supplierCollection.getSupplierById(this.model.get('supplier'));
            this.model.set('supplier', supplier.get('name'));
            this.$el.html(ich.template_income_file(this.model.toJSON()));
            return this;
        },
        onResult: function() {
            new ModalView({
                model: this.model
            });
        },
        onDelete: function() {
            if(confirm('Удалить эту запись?')) {
                this.model.destroy({
                    wait: true
                });
            }
        }
    });

    return ViewItem;
});