define([
    'jquery',
    'underscore',
    'backbone',
    'app/upload/model'
], function($, _, Backbone, IncomeModel) {
    var IncomeCollection = Backbone.Collection.extend({
        model: IncomeModel,
        url: 'invoice_income'
    });

    var incomeCollection = new IncomeCollection();

    return incomeCollection;
});