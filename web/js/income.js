require.config({
    paths: {
        jquery: 'lib/jquery',
        underscore: 'lib/underscore',
        backbone: 'lib/backbone',
        bootstrap: 'lib/bootstrap',
        ich: 'lib/ICanHaz.min',
        'bootstrap-datepicker-ru': 'lib/bootstrap-datepicker/bootstrap-datepicker.ru',
        'bootstrap-datepicker': 'lib/bootstrap-datepicker/bootstrap-datepicker'
    },
    shim: {
        'ich': {
            exports: 'ich'
        },
        'bootstrap': {
            deps: ['jquery'],
            exports: 'bootstrap'
        },
        'bootstrap-datepicker-ru': {
            deps: ['bootstrap-datepicker'],
            exports: 'bootstrap-datepicker-ru'
        },
        'bootstrap-datepicker': {
            deps: ['jquery'],
            exports: 'bootstrap-datepicker'
        }
    }
});

define([
    'jquery',
    'app/pageIncome'
], function($, pageIncome) {
    //$(window).load(function () {
        pageIncome.initialize();
    //});
});
