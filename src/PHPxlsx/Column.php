<?php
/**
 * User: sasha_000
 * Date: 04.11.2014
 * Time: 18:34
 */

namespace PHPxlsx;


class Column 
{
    private $name;

    private $data;

    private $attributes;

    public function __construct($name, $data, $attributes)
    {
        $this->name = $name;
        $this->data = $data;
        $this->attributes = $attributes;
    }

    /**
     * @return mixed
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    public function getLetter()
    {
        return preg_replace('/[0-9]+/', '', $this->name);
    }

} 