<?php
/**
 * User: sasha_000
 * Date: 04.11.2014
 * Time: 18:33
 */

namespace PHPxlsx;


class Row 
{
    private $columns;

    private $number;

    public function __construct($number, $columns = array())
    {
        $this->number = $number;
        $this->columns = $columns;
    }

    public function addColumn($column)
    {
        $this->columns[] = $column;
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    public function getColumn($letter)
    {
        if(array_key_exists($letter, $this->columns)) {
            return $this->columns[$letter];
        }

        return null;
    }

    public function hasColumn($letter)
    {
        if(array_key_exists($letter, $this->columns)) {
            return true;
        }

        return false;
    }

    public function isColumnEmpty($letter)
    {

    }

    public function getNumber()
    {
        return $this->number;
    }

} 