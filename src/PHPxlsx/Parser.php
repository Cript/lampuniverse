<?php

namespace PHPxlsx;

class Parser
{
    private $extractPath;

    private $sheet1;

    private $sharedStrings;

    public function __construct()
    {
        if(!extension_loaded('zip')) {
            throw new \RuntimeException('Activate \'zip\' PHP extension');
        }
    }

    public function parse($fileName)
    {
        if(false === file_exists($fileName)) {
            throw new ParseException(sprintf('File %s not found', $fileName));
        }

        if(false === $this->unzip($fileName)) {
            throw new ParseException(sprintf('ZIP error'));
        };

        $this->checkUnzipFiles();
        $this->parseSharedStrings();


        $data = $this->parseSheet($this->sheet1);

        return $data;

    }

    private function unzip($fileName)
    {
        $pathinfo = pathinfo($fileName);
        $extractPath = $pathinfo['dirname'].DIRECTORY_SEPARATOR.$pathinfo['filename'];
        $zip = new \ZipArchive();
        $this->extractPath = $extractPath;
        if(true === $zip->open($fileName)) {
            return $zip->extractTo($this->extractPath);
        };
    }

    private function checkUnzipFiles()
    {
        $this->sheet1 = $this->extractPath.DIRECTORY_SEPARATOR.'xl/worksheets/sheet1.xml';
        if(false === file_exists($this->sheet1)) {
            throw new ParseException(
                sprintf('File %s not found', $this->sheet1)
            );
        }
    }

    private function parseSharedStrings()
    {
        $sharedStringsFile = $this->extractPath.DIRECTORY_SEPARATOR.'xl/sharedStrings.xml';
        if(false === file_exists($sharedStringsFile)) {
            $this->sharedStrings = false;
            return;
        }
        $sheet = simplexml_load_file($sharedStringsFile);
        $sharedStrings = array();
        foreach($sheet->si as $sheetRow) {
            $columnData = '';
            foreach($sheetRow->r as $string) {
                $columnData .= (string) $string->t;
            }
            $sharedStrings[] = $columnData;
        }

        $this->sharedStrings = $sharedStrings;
    }

    private function getSharedString($key)
    {
        if(false === $this->sharedStrings) {
            throw new ParseException(
                sprintf('sharedStrings.xml not founded!')
            );
        }
        return $this->sharedStrings[$key];
    }

    private function parseSheet($name)
    {
        $sheet = simplexml_load_file($name);
        $rows = array();
        foreach($sheet->sheetData->row as $sheetRow) {
            $columns = $this->parseRow($sheetRow);

            $attributes = $sheetRow->attributes();
            $rowNumber = (string) $attributes->r;
            $rows[$rowNumber] = new Row($rowNumber, $columns);
        }
        return $rows;
    }

    private function parseRow(\SimpleXMLElement $row)
    {
        $columns = array();
        foreach($row->c as $sheetColumn)
        {
            $attributes = $sheetColumn->attributes();

            $columnName = (string) $attributes->r;
            $columnData = '';
            switch((string) $attributes->t) {
                case 'n':
                    $columnData = (string) $sheetColumn->v;
                    break;
                case 's':
                    $columnData = $this->getSharedString((int) $sheetColumn->v);
                    break;
                case 'inlineStr':
                    foreach($sheetColumn->is->r as $inlineStr) {
                        $columnData .= (string) $inlineStr->t;
                    }
                    break;
            }

            $column = new Column($columnName, $columnData, $attributes);

            $columns[$column->getLetter()] = $column;
        }
        return $columns;
    }

} 