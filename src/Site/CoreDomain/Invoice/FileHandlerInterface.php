<?php
/**
 * Created by PhpStorm.
 * User: sasha_000
 * Date: 11.10.2014
 * Time: 1:40
 */

namespace Site\CoreDomain\Invoice;


interface FileHandlerInterface
{
    public function exists($fileName);

    public function save($fileName, $format);
} 