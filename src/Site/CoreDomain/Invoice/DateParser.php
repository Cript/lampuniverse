<?php

namespace Site\CoreDomain\Invoice;

class DateParser 
{
    private $month = array(
        'января' => 1,
        'февраля' => 2,
        'марта' => 3,
        'апреля' => 4,
        'мая' => 5,
        'июня' => 6,
        'июля' => 7,
        'августа' => 8,
        'сентября' => 9,
        'октября' => 10,
        'ноября' => 11,
        'декабря' => 12
    );

    public function parse($date, $month, $year)
    {
        $month = $this->getMonthNumber($month);
        $date = $date.' '.$month.' '.$year;
        $date = \DateTime::createFromFormat('d m Y', $date);
        return $date;
    }

    private function getMonthNumber($month)
    {
        if(isset($this->month[$month])) {
            return $this->month[$month];
        }
        return $month;
    }
} 