<?php

namespace Site\CoreDomain\Invoice\ArticleParser;


class ArticleParser5 implements ArticleParserInterface
{
    /**
     * Удаляем все кирилические символы, делим строку по пробелам, возвращаем первый элемент
     *
     * Примеров нет, не было прайсов
     *
     * @param $fieldValue
     * @return mixed
     */
    public function parse($fieldValue)
    {
        $article = explode(' ',preg_replace('/^[\x{0410}-\x{042F}\s]+/iu', '', $fieldValue));
        return 'mw_'.$article[0];
    }

    public function parse2($fieldValue)
    {
        $article = explode(' ',preg_replace('/^[\x{0410}-\x{042F}\s]+/iu', '', $fieldValue));
        return 'ch_'.$article[0];
    }

} 