<?php
/**
 * User: sasha_000
 * Date: 17.11.2014
 * Time: 22:28
 */

namespace Site\CoreDomain\Invoice\ArticleParser;


interface ArticleParserInterface
{
    public function parse($fieldValue);
} 