<?php
/**
 * User: sasha_000
 * Date: 20.11.2014
 * Time: 10:42
 */

namespace Site\CoreDomain\Invoice\ArticleParser;


class NotFoundArticleException extends \Exception
{
    private $articles = array();

    public function setArticles($articles)
    {
        $this->articles = $articles;
    }

    public function getArticles()
    {
        return $this->articles;
    }
} 