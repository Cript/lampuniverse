<?php

namespace Site\CoreDomain\Invoice\ArticleParser;


class ArticleParser10 implements ArticleParserInterface
{
    /**
     * Удаляем все кирилические символы, делим строку по пробелам, возвращаем первый элемент
     *
     * Люстра ARM025-05-W (10115060/201213/0014873, КИТАЙ => ARM025-05-W
     * Настольная лампа ARM388-00-R (10115060/070714/0007877. КИТАЙ) => ARM388-00-R
     *
     * Затем прибавляем в начале артикула приставку
     *
     * @param $fieldValue
     * @return mixed
     */
    public function parse($fieldValue)
    {
        $article = explode(' ',preg_replace('/^[\x{0410}-\x{042F}\s]+/iu', '', $fieldValue));
        return 'eglo_'.$article[0];
    }

} 