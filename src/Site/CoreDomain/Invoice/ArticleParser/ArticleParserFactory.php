<?php

namespace Site\CoreDomain\Invoice\ArticleParser;

class ArticleParserFactory 
{
    public function getParser($supplierId)
    {
        $className = 'Site\CoreDomain\Invoice\ArticleParser\ArticleParser'.$supplierId;
        if(class_exists($className)) {
            $parser =  new $className;
        } else {
            $parser = new ArticleParserDefault();
        }
        return $parser;
    }
} 