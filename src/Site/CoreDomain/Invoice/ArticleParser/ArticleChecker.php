<?php
/**
 * User: sasha_000
 * Date: 20.11.2014
 * Time: 10:14
 */

namespace Site\CoreDomain\Invoice\ArticleParser;


use Site\CoreDomainBundle\Repository\ProductEntityRepository;

class ArticleChecker
{
    private $productEntityRepository;

    private $articleParserFactory = null;

    private $articleParser = null;

    public function __construct(
        ProductEntityRepository $productEntityRepository,
        ArticleParserFactory $articleParserFactory
    ) {
        $this->productEntityRepository = $productEntityRepository;
        $this->articleParserFactory = $articleParserFactory;
    }

    public function initArticleParser($supplierId)
    {
        $this->articleParser = $this->articleParserFactory->getParser($supplierId);
    }

    private function replaceSymbols($value)
    {
        $from = array(
            'А', 'О', 'С', 'Т'
        );
        $to = array(
            'A', 'O', 'C', 'T'
        );

        $value = str_replace($from, $to, $value);

        return $value;
    }

    public function getArticle($value)
    {
        if(null === $this->articleParserFactory) {
            throw new \Exception(sprintf('articleParserFactory not declared'));
        }

        $articleParserReflection = new \ReflectionClass($this->articleParser);
        $parseMethods = $articleParserReflection->getMethods(\ReflectionMethod::IS_PUBLIC);

        /**
         * Необходимо заменить символы кирилицы на латиницу.
         * После распознования abbyy ocrsdk такие одиночные символы распознаются как русские
         * Поэтому артикли не находятся в базе
         */
        $value = $this->replaceSymbols($value);


        $checkArticles = array();
        foreach($parseMethods as $parserMethod) {
            $article = $this->articleParser->{$parserMethod->getName()}($value);
            if($this->productEntityRepository->find($article) > 0) {
                return $article;
            };
            $checkArticles[] = $article;
        }

        $notFoundArticleException = new NotFoundArticleException(sprintf(
            'Артикль для продукта %s не найден. Проверены варианты: %s',
            $value,
            implode(', ', $checkArticles)
        ));
        $notFoundArticleException->setArticles($checkArticles);
        throw $notFoundArticleException;
    }
} 