<?php

namespace Site\CoreDomain\Invoice\ArticleParser;


class ArticleParserDefault implements ArticleParserInterface
{
    public function parse($fieldValue)
    {
	$article = explode(' ',preg_replace('/^[\x{0410}-\x{042F}\s]+/iu', '', $fieldValue));
        return $article[0];
    }
} 
