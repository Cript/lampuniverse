<?php

namespace Site\CoreDomain\Invoice\NumberParser;

class NumberParser
{

    private $numberZero;

    public function setNumberZero($numberZero)
    {
        if(null === $numberZero) {
            $numberZero = 3;
        }
        $this->numberZero = $numberZero;
    }

    public function parse($value)
    {
        $pattern = sprintf('/[0]{%s}$/', $this->numberZero);
        return preg_replace($pattern, '', $value);
    }
} 