<?php
/**
 * Created by PhpStorm.
 * User: sasha_000
 * Date: 03.10.2014
 * Time: 17:28
 */

namespace Site\CoreDomain\Invoice;


interface ProductRepository
{
    /**
     * Возвращает список необработанных счет-фактур
     *
     * @return array
     */
    public function findByInvoice(Invoice $invoice);

    /**
     * Сохраняет новую счет-фактуру
     *
     * @param Product $product
     */
    public function add(Product $product);

    /**
     * Удаляет счет-фактуру
     *
     * @param Product $product
     */
    public function remove(Product $product);

    /**
     * Обновляет счет-фактуру
     *
     */
    public function update();
} 