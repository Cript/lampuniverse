<?php
/**
 * Created by PhpStorm.
 * User: sasha_000
 * Date: 03.10.2014
 * Time: 17:25
 */

namespace Site\CoreDomain\Invoice;


use Site\CoreDomain\Invoice\Parser\ParserInterface;
use Site\CoreDomain\Invoice\Services\ProcessingInvoice;
use Symfony\Component\Validator\Constraints\DateTime;

class Invoice
{
    public function __construct($fileName, $originalFileName)
    {
        $this->fileName = $fileName;
        $this->originalFileName = $originalFileName;
        $this->loadTime = new \DateTime();
        $this->status = 'new';
    }

    protected $id;

    protected $fileName;

    protected $parseFileName;

    protected $originalFileName;

    protected $loadTime;

    protected $parseTime;

    protected $supplier;

    protected $number;

    protected $date;

    protected $total;

    protected $totalTax;

    protected $status;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @return string
     */
    public function getParseFileName()
    {
        return $this->parseFileName;
    }

    /**
     * @return mixed
     */
    public function getOriginalFileName()
    {
        return $this->originalFileName;
    }

    /**
     * @return mixed
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * @return \DateTime
     */
    public function getLoadTime()
    {
        return $this->loadTime;
    }

    /**
     * @return \DateTime
     */
    public function getParseTime()
    {
        return $this->parseTime;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @return mixed
     */
    public function getTotalTax()
    {
        return $this->totalTax;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function parse(ParserInterface $parser, $supplierId)
    {
        $this->parseFileName = $parser->parse($this->fileName);
        $this->supplier = $supplierId;
        $this->parseTime = new \DateTime();
        $this->status = 'parsed';
    }

    public function parse2(ProcessingInvoice $processingInvoice)
    {
        $this->date = $processingInvoice->getDate();
        $this->number = $processingInvoice->getNumber();
        $this->total = $processingInvoice->getTotal();
        $this->totalTax = $processingInvoice->getTotalTax();
        $this->status = 'processed';
    }

    public function checkSave($number, $date, $total, $totalTax, $updateStatus = false)
    {
        $this->number = $number;
        $this->date = $date;
        $this->total = $total;
        $this->totalTax = $totalTax;
        if(true === $updateStatus) {
            $this->status = 'income';
        }
    }
}