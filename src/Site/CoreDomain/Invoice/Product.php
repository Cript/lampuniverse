<?php
/**
 * User: sasha_000
 * Date: 04.11.2014
 * Time: 20:44
 */

namespace Site\CoreDomain\Invoice;


class Product
{
    protected $id;

    protected $invoice;

    protected $article;

    protected $articleFind;

    protected $name;

    protected $price;

    protected $number;

    protected $total;

    protected $totalTax;

    protected $GTDNumber;

    protected $countryCode;

    protected $countryName;

    protected $sort;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @return mixed
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @return mixed
     */
    public function getArticleFind()
    {
        return $this->articleFind;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @return mixed
     */
    public function getTotalTax()
    {
        return $this->totalTax;
    }

    /**
     * @return mixed
     */
    public function getGTDNumber()
    {
        return $this->GTDNumber;
    }

    /**
     * @return mixed
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @return mixed
     */
    public function getCountryName()
    {
        return $this->countryName;
    }

    /**
     * @return mixed
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param null $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    function __construct($article, $articleFind, $name, $price, $number, $total, $totalTax, $GTDNumber,
        $countryCode, $countryName, $sort = null)
    {
        $this->article = $article;
        $this->articleFind = $articleFind;
        $this->countryCode = $countryCode;
        $this->countryName = $countryName;
        $this->GTDNumber = $GTDNumber;
        $this->name = $name;
        $this->number = $number;
        $this->price = $price;
        $this->total = $total;
        $this->totalTax = $totalTax;
        $this->sort = $sort;
    }

    public function saveInvoice($article, $articleFind, $price, $number, $countryName, $GTDNumber, $total, $totalTax)
    {
        $this->article = $article;
        $this->articleFind = $articleFind;
        $this->countryName = $countryName;
        $this->GTDNumber = $GTDNumber;
        $this->number = $number;
        $this->price = $price;
        $this->total = $total;
        $this->totalTax = $totalTax;
    }
} 