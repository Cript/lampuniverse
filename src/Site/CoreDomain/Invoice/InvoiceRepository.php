<?php

namespace Site\CoreDomain\Invoice;


interface InvoiceRepository
{
    /**
     * Возвращает счет-фактуру по id
     *
     * @param integer $id
     * @return Invoice|null
     */
    public function find($id);

    /**
     * Сохраняет новую счет-фактуру
     *
     * @param Invoice $invoice
     */
    public function add(Invoice $invoice);

    /**
     * Удаляет счет-фактуру
     *
     * @param Invoice $invoice
     */
    public function remove(Invoice $invoice);

    /**
     * Обновляет счет-фактуру
     */
    public function update();

    /**
     * Возвращает список необработанных счет-фактур
     *
     * @return array
     */
    public function findNewAll();

    /**
     * Возвращает список обработанных счет-фактур
     *
     * @return array
     */
    public function findParseAll();
} 