<?php

namespace Site\CoreDomain\Invoice\Services;
use PHPxlsx\Row;
use Site\CoreDomain\Invoice\ArticleParser\ArticleChecker;
use Site\CoreDomain\Invoice\ArticleParser\NotFoundArticleException;
use Site\CoreDomain\Invoice\DataPreparation\PricePreparer;
use Site\CoreDomain\Invoice\DateParser;
use Site\CoreDomain\Invoice\InvoiceRepository;
use Site\CoreDomain\Invoice\NumberParser\NumberParser;
use Site\CoreDomainBundle\Entity\Product;

class ProcessingInvoice
{
    private $repository;

    private $articleChecker;

    private $numberParser;

    private $dateParser;

    private $pricePreparer;

    private $products = array();

    private $total;

    private $totalTax;

    private $number;

    private $date;

    public function __construct(
        InvoiceRepository $repository,
        ArticleChecker $articleChecker,
        NumberParser $numberParser,
        DateParser $dateParser,
        PricePreparer $pricePreparer
    ) {
        $this->repository = $repository;
        $this->articleChecker = $articleChecker;
        $this->numberParser = $numberParser;
        $this->dateParser = $dateParser;
        $this->pricePreparer = $pricePreparer;
    }

    public function handle($data, $supplierId, $zeroNumber)
    {
        $this->articleChecker->initArticleParser($supplierId);
        $this->numberParser->setNumberZero($zeroNumber);

        /**
         * @var $row \PHPxlsx\Row
         */
        $productNextRow = false;
        $sort = 1;
        foreach($data as $row) {
            $columnValues = array_values($row->getColumns());
            $firstColumn = $columnValues[0];

            /**
             * Строка
             * Счет-фактура № S/000029586 от 04 декабря 2014 г.
             */
            if ($firstColumn->getLetter() == 'A' and
                preg_match('/^\s*счет-фактура\s[№N]\s*(\S+)\sот\s(\d{1,2})[\s|\.](\S+)[\s|\.](\d{2,4})\s*.*$/iu', $row->getColumn('A')->getData(), $matches) > 0
            ) {
                $this->processNumberDateRow($matches);
                continue;
            }

            /**
             * Строка
             * Наименование товара (описание выполненных работ, оказанных услуг), имущественного права
             */
            if ($firstColumn->getLetter() == 'A' and substr_count(
                    $firstColumn->getData(),
                    'Наименование товара'
                )
            ) {
                $productNextRow = 3;
                continue;
            }



            if($productNextRow > 0) {
                $productNextRow--;
            }

            /**
             * Строка
             * Всего к оплате
             */
            if ($firstColumn->getLetter() == 'A' and substr_count(
                    $firstColumn->getData(),
                    'Всего к оплате'
                )
            ) {
                $this->processTotalRow($row);
                continue;
            }

            /**
             * Строка товара
             */
            if (false !== $productNextRow and$productNextRow <= 0) {
                if(false === $this->isProductRow($row)) {
                    continue;
                }

                $product = $this->processProductRow($row, $sort);
                $sort++;
                if($product instanceof Product) {
                    $this->products[] = $product;
                }
            }
        }
    }

    private function isProductRow(Row $row)
    {
        if(
            $row->hasColumn('A') and
            $row->hasColumn('E') and
            $row->hasColumn('D') and
            $row->hasColumn('F') and
            $row->hasColumn('J') and
            $row->hasColumn('M') and
            $row->hasColumn('K') and
            $row->hasColumn('L')
        ) {
            return true;
        }
        return false;
    }

    private function checkProductColumns(Row $row)
    {
        return true;
        $columnAValue = $row->getColumn('A')->getData();
        $columnEValue = $row->getColumn('E')->getData();
        $columnDValue = $row->getColumn('D')->getData();
        $columnFValue = $row->getColumn('F')->getData();
        $columnJValue = $row->getColumn('J')->getData();
        if(
            !empty($columnAValue) and
            !empty($columnEValue) and
            !empty($columnDValue) and
            !empty($columnFValue) and
            !empty($columnJValue)
        ) {
            return true;
        }
        return false;
    }


    private function processTotalRow(Row $row)
    {
        $this->total = (float) str_replace(array(' ', ','), array('', '.'), $row->getColumn('F')->getData());
        $this->totalTax = (float) str_replace(array(' ', ','), array('', '.'), $row->getColumn('J')->getData());
    }

    private function processProductRow(Row $row, $sort)
    {
        $articleStatus = true;
        try {
            $article = $this->articleChecker->getArticle((string)$row->getColumn('A')->getData());
        } catch(NotFoundArticleException $e) {
            $article = implode(', ',$e->getArticles());
            $articleStatus = false;
        }

        if($this->checkProductColumns($row)) {
            $product = new Product(
                $article,
                $articleStatus,
                $row->getColumn('A')->getData(),
                (float) $this->pricePreparer->process($row->getColumn('E')->getData()),
                $this->numberParser->parse((int) $row->getColumn('D')->getData()),
                (float) $this->pricePreparer->process($row->getColumn('F')->getData()),
                (float) $this->pricePreparer->process($row->getColumn('J')->getData()),
                str_replace(' ', '', $row->getColumn('M')->getData()),
                (int) $row->getColumn('K')->getData(),
                $row->getColumn('L')->getData(),
                $sort
            );
            return $product;
        } else {
            return false;
        }
    }

    private function processNumberDateRow($matches)
    {
        $this->number = $matches[1];
        $this->date = $this->dateParser->parse($matches[2], $matches[3], $matches[4]);
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @return mixed
     */
    public function getTotalTax()
    {
        return $this->totalTax;
    }

    /**
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }
}
