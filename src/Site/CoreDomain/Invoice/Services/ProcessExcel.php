<?php

namespace Site\CoreDomain\Invoice\Services;


use Site\CoreDomain\Invoice\Invoice;
use Site\CoreDomainBundle\Repository\SupplierRepository;

class ProcessExcel
{
    private $supplierRepository;

    public function __construct(SupplierRepository $supplierRepository)
    {
        $this->supplierRepository = $supplierRepository;
    }

    public function process(Invoice $invoice)
    {
        $supplier = $this->supplierRepository->find($invoice->getSupplier());
        $products = $invoice->getProducts();

        $templateFile = realpath(__DIR__.'/../Resources/templates/template.xlsx');

        $excel2 = \PHPExcel_IOFactory::createReader('Excel2007');
        $excel2 = $excel2->load($templateFile);
        $excel2->setActiveSheetIndex(0);

        $excel2->getActiveSheet()
            ->setCellValue('A2', sprintf('Счет-фактура № %s от %s г.', $invoice->getNumber(), $invoice->getDate()->format('d.n.Y')))
            ->setCellValue('A4', sprintf('Продавец: %s', $supplier['name']));

        /**
         * @var $product \Site\CoreDomain\Invoice\Product
         */
        $line = 17;
        foreach($products as $product) {
            $excel2->getActiveSheet()
                ->setCellValue('A'.$line, $product->getArticle())
                ->setCellValue('C'.$line, 'шт.')
                ->setCellValue('D'.$line, $product->getNumber())
                ->setCellValue('E'.$line, $product->getPrice())
                ->setCellValue('F'.$line, $product->getTotal())
                ->setCellValue('G'.$line, 'без акциза')
                ->setCellValue('H'.$line, '0,18')
                ->setCellValue('K'.$line, $product->getTotalTax())
                ->setCellValue('L'.$line, $product->getCountryCode())
                ->setCellValue('M'.$line, $product->getCountryName())
                ->setCellValue('N'.$line, $product->getGTDNumber())
                ;
            ++$line;
        }

        $excel2->getActiveSheet()
            ->setCellValue('A'.$line, 'Всего к оплате')
            ->setCellValue('K'.$line, $invoice->getTotalTax());

        $objWriter = \PHPExcel_IOFactory::createWriter($excel2, 'Excel2007');
	$tmpFile = stream_get_meta_data(tmpfile());
        $tmpFileName = $tmpFile['uri'];
        $objWriter->save($tmpFileName);

        return $tmpFileName;
    }
}
