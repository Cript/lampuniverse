<?php
/**
 * Short description for class
 *
 * @author     Aleksandr Boroduliun <sashaborodulin@gmail.com>
 */

namespace Site\CoreDomain\Invoice\DataPreparation;

class PricePreparer implements PreparationInterface
{
    private $data;

    public function process($data)
    {
        $this->data = $data;

        $this->removeSpaces();
        $this->replaceComma();
        $this->replaceApostrophe();

        return $this->data;
    }

    private function removeSpaces()
    {
        $this->data = str_replace(' ', '', $this->data);
    }

    private function replaceComma()
    {
        $this->data = str_replace(',', '.', $this->data);
    }

    private function replaceApostrophe()
    {
        $this->data = str_replace('\'', '', $this->data);
    }
}