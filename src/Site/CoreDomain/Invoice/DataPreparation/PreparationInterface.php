<?php
/**
 * Short description for class
 *
 * @author     Aleksandr Boroduliun <sashaborodulin@gmail.com>
 */

namespace Site\CoreDomain\Invoice\DataPreparation;

interface PreparationInterface
{
    public function process($data);
}