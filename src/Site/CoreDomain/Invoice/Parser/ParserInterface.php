<?php

namespace Site\CoreDomain\Invoice\Parser;

interface ParserInterface
{
    public function parse($fileName, $format = 'xlsx');
} 