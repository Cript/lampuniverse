<?php

namespace Site\CoreDomain\Invoice\Parser;

use Site\CoreDomain\Invoice\FileHandlerInterface;

class AbbyyOCRParser implements ParserInterface
{
    private $applicationId;

    private $password;

    private $fileHandler;

    private $format;

    public function __construct(FileHandlerInterface $fileHandler, $applicationId, $password)
    {
        $this->fileHandler = $fileHandler;
        $this->applicationId = $applicationId;
        $this->password = $password;
    }

    public function parse($fileName, $format = 'xlsx')
    {
        $this->format = $format;

        if(false === $this->fileHandler->exists($fileName)) {
            throw new \Exception('File %s not found', $fileName);
        };
        $fileName = $this->fileHandler->getFullName($fileName);
        $url = $this->processImage($fileName);
        return $this->fileHandler->save($url->__toString(), $this->format);
    }

    private function curlFileCreate($filename, $mimetype = '', $postname = '')
    {
        if (!function_exists('curl_file_create')) {
            return "@$filename;filename="
            . ($postname ?: basename($filename))
            . ($mimetype ? ";type=$mimetype" : '');
        }
        return curl_file_create($filename);
    }


    private function processImage($fileName)
    {
        $url = sprintf('http://cloud.ocrsdk.com/processImage?language=English,Russian&exportFormat=%s', $this->format);

        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandle, CURLOPT_USERPWD, "$this->applicationId:$this->password");
        curl_setopt($curlHandle, CURLOPT_POST, 1);
        curl_setopt($curlHandle, CURLOPT_USERAGENT, "PHP Cloud OCR SDK Sample");

        $post_array = array(
            "my_file" => $this->curlFileCreate($fileName),
        );
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $post_array);
        $response = curl_exec($curlHandle);

        if($response == FALSE) {
            $errorText = curl_error($curlHandle);
            curl_close($curlHandle);
            throw new \Exception($errorText);
        }

        $httpCode = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
        curl_close($curlHandle);

        $xml = simplexml_load_string($response);
        if($httpCode != 200) {
            if(property_exists($xml, "message")) {
                throw new \Exception($xml->message);
            }
            die("unexpected response ".$response);
        }

        $arr = $xml->task[0]->attributes();
        $taskStatus = $arr["status"];
        if($taskStatus != "Queued") {
            throw new \Exception("Unexpected task status ".$taskStatus);
        }

        return $this->taskStatus($arr["id"]);
    }

    private function taskStatus($taskID)
    {
        $url = sprintf('http://cloud.ocrsdk.com/getTaskStatus?taskid=%s', $taskID);
        while(true)
        {
            sleep(5);
            $curlHandle = curl_init();
            curl_setopt($curlHandle, CURLOPT_URL, $url);
            curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curlHandle, CURLOPT_USERPWD, "$this->applicationId:$this->password");
            curl_setopt($curlHandle, CURLOPT_USERAGENT, "PHP Cloud OCR SDK Sample");
            $response = curl_exec($curlHandle);
            $httpCode = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);
            curl_close($curlHandle);

            // parse xml
            $xml = simplexml_load_string($response);
            if($httpCode != 200) {
                if(property_exists($xml, "message")) {
                    throw new \Exception($xml->message);
                }
                throw new \Exception("Unexpected response ".$response);
            }
            $arr = $xml->task[0]->attributes();
            $taskStatus = $arr["status"];
            if($taskStatus == "Queued" || $taskStatus == "InProgress") {
                // continue waiting
                continue;
            }
            if($taskStatus == "Completed") {
                // exit this loop and proceed to handling the result
                break;
            }
            if($taskStatus == "ProcessingFailed") {
                throw new \Exception("Task processing failed: ".$arr["error"]);
            }
            throw new \Exception("Unexpected task status ".$taskStatus);
        }

        // Result is ready. Download it

        //$url = $arr["resultUrl"];
        /*$curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
        // Warning! This is for easier out-of-the box usage of the sample only.
        // The URL to the result has https:// prefix, so SSL is required to
        // download from it. For whatever reason PHP runtime fails to perform
        // a request unless SSL certificate verification is off.
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($curlHandle);
        curl_close($curlHandle);*/

        return $arr["resultUrl"];
    }

} 