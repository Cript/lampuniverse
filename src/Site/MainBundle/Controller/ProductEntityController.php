<?php

namespace Site\MainBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ProductEntityController extends Controller
{
    public function findAction(Request $request)
    {
        $q = $request->query->get('q', null);
        if(null === $q) {
            return new JsonResponse(array(
                'error' => 'Параметр поиска пустой'
            ), 400);
        }

        $productEntityRepository = $this->get('product_entity_repository');
        $result = $productEntityRepository->searchByArticle($q);

        return new JsonResponse($result);
    }
} 