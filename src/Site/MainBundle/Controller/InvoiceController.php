<?php

namespace Site\MainBundle\Controller;

use PHPxlsx\Parser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Site\CoreDomainBundle\Entity\Invoice;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Site\CoreDomain\Invoice\Parser\AbbyyOCRParser;

class InvoiceController extends Controller
{
    /**
     * @var $repositoryInvoice \Site\CoreDomain\Invoice\Invoice
     */

    /**
     * Возвращает список неообработанных файлов
     *
     * @return JsonResponse
     */
    public function getAction()
    {
        $repositoryInvoice = $this->get('invoice_repository');
        $invoices = $repositoryInvoice->findNewAll();

        /**
         * @var $invoice Invoice
         */
        $data = array();
        foreach ($invoices as $invoice) {
            $data[] = array(
                'id' => $invoice->getId(),
                'link' => '/files/pdf/' . $invoice->getFileName(),
                'originalName' => $invoice->getOriginalFileName(),
                'loadTime' => $invoice->getLoadTime()->format('d.m.Y H:i')
            );
        }

        return new JsonResponse($data);
    }

    public function postAction()
    {
        $form = $this->createForm('invoice');
        $form->handleRequest($this->getRequest());
        if(false === $form->isValid()) {
            return new JsonResponse(
                array(
                    'error' => $form->getErrors(true)->__toString()
                )
            );
        }

        $repositoryInvoice = $this->get('invoice_repository');

        /**
         * @var $file \Symfony\Component\HttpFoundation\File\UploadedFile
         */
        $file = $form->getData()->getFile();
        $fileName = uniqid().'.'.$file->guessExtension();

        try {
            $file->move($this->get('kernel')->getRootDir() . '/../web/files/pdf/', $fileName);
        } catch (FileException $e) {
            return new JsonResponse(array(
                'error' => $e->getMessage()
            ));
        }
        /**-------------------------------------------*/

        $invoice = new Invoice($fileName, $file->getClientOriginalName());
        $repositoryInvoice->add($invoice);

        return new JsonResponse(
            array(
                'id' => $invoice->getId(),
                'link' => '/files/pdf/' . $fileName,
                'originalName' => $invoice->getOriginalFileName(),
                'loadTime' => $invoice->getLoadTime()->format('d.m.Y H:i')
            )
        );
    }

    public function putAction($id)
    {
        $repositoryInvoice = $this->get('invoice_repository');
        $invoice = $repositoryInvoice->find($id);
        if($invoice === null) {
            throw $this->createNotFoundException();
        }

        /**
         * @var $invoice Invoice
         */
        $data = json_decode(file_get_contents("php://input"), true);
        $data['date'] = \DateTime::createFromFormat('d.m.Y', $data['date']);
        $invoice->checkSave(
            $data['number'],
            $data['date'],
            $data['total'],
            $data['totalTax'],
            isset($data['updateStatus']) ? : false
        );

        $repositoryInvoice->update();

        return new JsonResponse();
    }

    public function deleteAction($id)
    {
        $repositoryInvoice = $this->get('invoice_repository');
        /**
         * @var $invoice Invoice
         */
        $invoice = $repositoryInvoice->find($id);
        if($invoice === null) {
            throw $this->createNotFoundException();
        }
        $repositoryInvoice->remove($invoice);
        $this->get('file_handler')->delete($invoice->getFileName());
        if(null !== $invoice->getParseFileName()) {
            $this->get('file_handler')->delete($invoice->getParseFileName());
        }

        //unlink($this->get('kernel')->getRootDir() . '/../web/files/pdf/'.$invoice->getFileName());

        return new JsonResponse();
    }

    /**
     * Распознование pdf файла
     * @param $id
     * @return JsonResponse
     */
    public function parseAction($id)
    {
        $repositoryInvoice = $this->get('invoice_repository');

        $supplierId = $this->getRequest()->query->get('supplierId', false);
        /**
         * @var $invoice Invoice
         */
        $invoice = $repositoryInvoice->find($id);
        if($invoice === null) {
            throw $this->createNotFoundException();
        }

        $parser = $this->get('parser');
        $invoice->parse($parser, $supplierId);

        $repositoryInvoice->update($invoice);

        return new JsonResponse(
            array(
                'supplier' => $invoice->getSupplier(),
                'parseLink' => $this->get('file_handler')->getWebLink($invoice->getParseFileName()),
                'parseTime' => $invoice->getParseTime()->format('d.m.Y H:i'),
                'parsed' => ($invoice->getStatus() == 'parsed') ? true : false,
                'processed' => ($invoice->getStatus() == 'processed') ? true : false
            )
        );
    }


    /**
     * Возвращает распознанные файлы
     * @return JsonResponse
     */
    public function getParseAction()
    {
        $repositoryInvoice = $this->get('invoice_repository');
        $invoices = $repositoryInvoice->findParseAll();

        /**
         * @var $invoice Invoice
         */
        $data = array();
        foreach ($invoices as $invoice) {
            $date = (null == $invoice->getDate()) ? null : $invoice->getDate()->format('d.m.Y');
            $data[] = array(
                'id' => $invoice->getId(),
                'link' => '/files/pdf/' . $invoice->getFileName(),
                'parseLink' => $this->get('file_handler')->getWebLink($invoice->getParseFileName()),
                'originalName' => $invoice->getOriginalFileName(),
                'supplier' => $invoice->getSupplier(),
                'loadTime' => $invoice->getLoadTime()->format('d.m.Y H:i'),
                'parseTime' => $invoice->getParseTime()->format('d.m.Y H:i'),
                'date' => $date,
                'number' => $invoice->getNumber(),
                'total' => $invoice->getTotal(),
                'totalTax' => $invoice->getTotalTax(),
                'parsed' => ($invoice->getStatus() == 'parsed') ? true : false,
                'processed' => ($invoice->getStatus() == 'processed') ? true : false
            );
        }

        return new JsonResponse($data);
    }

    /**
     * Возвращает поступления
     * @return JsonResponse
     */
    public function getIncomeAction(Request $request)
    {
        $repositoryInvoice = $this->get('invoice_repository');

        $conditions = $request->query->all();
        $invoices = $repositoryInvoice->findIncome($conditions);

        /**
         * @var $invoice Invoice
         */
        $data = array();
        foreach ($invoices as $invoice) {
            $data[] = array(
                'id' => $invoice->getId(),
                'link' => '/files/pdf/' . $invoice->getFileName(),
                'parseLink' => $this->get('file_handler')->getWebLink($invoice->getParseFileName()),
                'originalName' => $invoice->getOriginalFileName(),
                'supplier' => $invoice->getSupplier(),
                'loadTime' => $invoice->getLoadTime()->format('d.m.Y H:i'),
                'parseTime' => $invoice->getParseTime()->format('d.m.Y H:i'),
                'date' => $invoice->getDate()->format('d.m.Y'),
                'number' => $invoice->getNumber(),
                'total' => $invoice->getTotal(),
                'parsed' => ($invoice->getStatus() == 'parsed') ? true : false,
                'processed' => ($invoice->getStatus() == 'processed') ? true : false,
                'income' => ($invoice->getStatus() == 'income') ? true : false
            );
        }

        return new JsonResponse($data);
    }

    /**
     * Обработка распознанных счет-фактур
     *
     * @param $id
     * @return JsonResponse
     */
    public function getParseDataAction($id)
    {

        $repositoryInvoice = $this->get('invoice_repository');
        /**
         * @var $invoice Invoice
         */
        $invoice = $repositoryInvoice->find($id);
        if($invoice === null) {
            throw $this->createNotFoundException();
        }

        $parserXLSX = $this->get('parser_data');

        $fileName = $this->get('file_handler')->getFullName($invoice->getParseFileName());
        $data = $parserXLSX->parse($fileName);

        $handler = $this->get('processing_invoice');

        /*
         * Запрос поставщика из базы для получения количества нулей после запятой
         */
        $supplier = $this->get('supplier_repository')->find($invoice->getSupplier());

        $handler->handle($data, $invoice->getSupplier(), $supplier['number_zeros']);

        $invoice->parse2($handler);

        foreach($handler->getProducts() as $product) {
            $product->setInvoice($invoice);
            $invoice->addProduct($product);
        }
        $repositoryInvoice->update();

        return new JsonResponse(array(
            'date' => $invoice->getDate()->format('d.m.Y'),
            'number' => $invoice->getNumber(),
            'total' => $invoice->getTotal(),
            'totalTax' => $invoice->getTotalTax(),
            'parsed' => ($invoice->getStatus() == 'parsed') ? true : false,
            'processed' => ($invoice->getStatus() == 'processed') ? true : false
        ));
    }

    public function getExcelAction($id)
    {
        $repositoryInvoice = $this->get('invoice_repository');
        /**
         * @var $invoice Invoice
         */
        $invoice = $repositoryInvoice->find($id);
        if($invoice === null) {
            throw $this->createNotFoundException();
        }

        $processingExcel = $this->get('processing_excel');

        $fileName = $processingExcel->process($invoice);

        $response = new Response(file_get_contents($fileName), 200, array(
            'Content-type' => 'application/vnd.ms-excel',
            'Content-Disposition' => sprintf('attachment; filename="invoice_%s_excel.xlsx"', $invoice->getNumber())
        ));
        return $response;
    }
}