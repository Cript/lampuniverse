<?php

namespace Site\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class SupplierController extends Controller
{
    public function getAction()
    {
        $supplierRepository = $this->get('supplier_repository');
        $suppliers = $supplierRepository->findAllActive();
        return new JsonResponse($suppliers);
    }
}