<?php

namespace Site\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $invoiceRepository = $this->get('invoice_repository');
        return $this->render('SiteMainBundle:Default:index.html.twig');
    }

    public function uploadPDFAction()
    {
        $form = $this->createForm('invoice', null, array(
            'action' => $this->generateUrl('site_main_invoice_post')
        ));

        return $this->render('SiteMainBundle:Default:uploadPDF.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function incomeAction()
    {
        return $this->render('SiteMainBundle:Default:income.html.twig');
    }
}
