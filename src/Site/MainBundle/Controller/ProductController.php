<?php
/**
 * Created by PhpStorm.
 * User: sasha_000
 * Date: 05.10.2014
 * Time: 1:00
 */

namespace Site\MainBundle\Controller;

use Site\CoreDomainBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends Controller
{
    /**
     * Возвращает список товаров
     * @return JsonResponse
     */
    public function getAction()
    {
        $id = $this->get('request')->query->get('invoice', false);
        if(false === $id) {
            throw $this->createNotFoundException('Не указан id счет-фактуры');
        }

        $repositoryInvoice = $this->get('invoice_repository');
        /**
         * @var $invoice Invoice
         */
        $invoice = $repositoryInvoice->find($id);
        if($invoice === null) {
            throw $this->createNotFoundException();
        }

        $productRepository = $this->get('product_repository');
        $products = $productRepository->findByInvoice($invoice);

        $data = array();
        foreach ($products as $product) {
            $data[] = array(
                'id' => $product->getId(),
                'article' => $product->getArticle(),
                'articleFind' => $product->getArticleFind(),
                'price' => $product->getPrice(),
                'number' => $product->getNumber(),
                'total' => $product->getTotal(),
                'totalTax' => $product->getTotalTax(),
                'countryName' => $product->getCountryName(),
                'GTDNumber' => $product->getGTDNumber()
            );
        }

        return new JsonResponse($data);
    }

    /**
     * Добавление нового товара
     */
    public function postAction(Request $request)
    {
        $data = $request->getContent();
        if(!empty($data)) {
            $data = json_decode($data, true);
        }

        $repositoryInvoice = $this->get('invoice_repository');
        /**
         * @var $invoice Invoice
         */
        $invoice = $repositoryInvoice->find($data['invoiceId']);
        if($invoice === null) {
            throw $this->createNotFoundException();
        }

        $productRepository = $this->get('product_repository');

        $product = new Product(
            $data['article'], $data['articleFind'], 'Добавлен вручную', $data['price'], $data['number'],
            $data['total'],  $data['totalTax'], $data['GTDNumber'], $data['countryCode'], $data['countryName']
        );
        $product->setInvoice($invoice);

        $productRepository->add($product);

        return new JsonResponse(array(
            'id' => $product->getId()
        ));
    }

    /**
     * Обновление товара
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function putAction(Request $request, $id)
    {
        $productRepository = $this->get('product_repository');
        $product = $productRepository->find($id);
        if(null === $product) {
            throw $this->createNotFoundException('Товар не найден');
        }
        /**
         * @var $product \Site\CoreDomain\Invoice\Product
         */

        $data = json_decode(file_get_contents("php://input"), true);

        $product->saveInvoice(
            $data['article'],
            $data['articleFind'],
            $data['price'],
            $data['number'],
            $data['countryName'],
            $data['GTDNumber'],
            $data['total'],
            $data['totalTax']
        );

        $productRepository->update();

        return new JsonResponse(array());
    }

    /**
     * Удаление товара
     *
     * @param $id
     * @return JsonResponse
     */
    public function removeAction($id)
    {
        $productRepository = $this->get('product_repository');
        $product = $productRepository->find($id);
        $productRepository->remove($product);

        return new JsonResponse();
    }
}