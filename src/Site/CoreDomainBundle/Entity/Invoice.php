<?php
/**
 * Created by PhpStorm.
 * User: sasha_000
 * Date: 03.10.2014
 * Time: 17:34
 */

namespace Site\CoreDomainBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Site\CoreDomain\Invoice\Invoice as InvoiceModel;

class Invoice extends InvoiceModel
{
    private $products;

    public function __construct($fileName, $originalFileName)
    {
        parent::__construct($fileName, $originalFileName);
        $this->products = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set loadTime
     *
     * @param \DateTime $loadTime
     * @return Invoice
     */
    public function setLoadTime($loadTime)
    {
        $this->loadTime = $loadTime;

        return $this;
    }

    /**
     * Get loadTime
     *
     * @return \DateTime 
     */
    public function getLoadTime()
    {
        return $this->loadTime;
    }

    /**
     * Set parseTime
     *
     * @param \DateTime $parseTime
     * @return Invoice
     */
    public function setParseTime($parseTime)
    {
        $this->parseTime = $parseTime;

        return $this;
    }

    /**
     * Get parseTime
     *
     * @return \DateTime 
     */
    public function getParseTime()
    {
        return $this->parseTime;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     * @return Invoice
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string 
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set parseFileName
     *
     * @param string $parseFileName
     * @return Invoice
     */
    public function setParseFileName($parseFileName)
    {
        $this->parseFileName = $parseFileName;

        return $this;
    }

    /**
     * Get parseFileName
     *
     * @return string 
     */
    public function getParseFileName()
    {
        return $this->parseFileName;
    }

    /**
     * Set originalFileName
     *
     * @param string $originalFileName
     * @return Invoice
     */
    public function setOriginalFileName($originalFileName)
    {
        $this->originalFileName = $originalFileName;

        return $this;
    }

    /**
     * Get originalFileName
     *
     * @return string 
     */
    public function getOriginalFileName()
    {
        return $this->originalFileName;
    }

    /**
     * Set supplier
     *
     * @param integer $supplier
     * @return Invoice
     */
    public function setSupplier($supplier)
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return integer 
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Set date
     *
     * @param string $date
     * @return Invoice
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return string 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return Invoice
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set total
     *
     * @param string $total
     * @return Invoice
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Add products
     *
     * @param \Site\CoreDomainBundle\Entity\Product $products
     * @return Invoice
     */
    public function addProduct(\Site\CoreDomainBundle\Entity\Product $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \Site\CoreDomainBundle\Entity\Product $products
     */
    public function removeProduct(\Site\CoreDomainBundle\Entity\Product $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts()
    {
        return $this->products;
    }
}
