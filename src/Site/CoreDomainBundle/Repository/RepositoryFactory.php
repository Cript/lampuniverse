<?php
/**
 * Created by PhpStorm.
 * User: sasha_000
 * Date: 03.10.2014
 * Time: 22:39
 */

namespace Site\CoreDomainBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Registry;


class RepositoryFactory
{
    private $doctrine;

    public function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function get($repositoryClass)
    {
        return $this->doctrine->getRepository($repositoryClass);
    }

} 