<?php

namespace Site\CoreDomainBundle\Repository;

use Doctrine\DBAL\Connection;

/**
 * Class SupplierRepository
 * @package Site\CoreDomainBundle\Repository
 */
class ProductEntityRepository
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function find($sku)
    {
        $result = $this->connection->executeQuery('SELECT COUNT(*) FROM catalog_product_entity WHERE sku = ?',
            array($sku),
            array(\PDO::PARAM_STR)
        )->fetch(\PDO::FETCH_COLUMN);

        return $result;
    }

    public function searchByArticle($q)
    {
        $result = $this->connection->executeQuery('SELECT sku FROM catalog_product_entity WHERE sku LIKE ?',
            array('%'.$q.'%'),
            array(\PDO::PARAM_STR)
        )->fetchAll();

        return $result;
    }
} 