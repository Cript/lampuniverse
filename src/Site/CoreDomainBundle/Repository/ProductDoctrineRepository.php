<?php

namespace Site\CoreDomainBundle\Repository;

use Site\CoreDomain\Invoice\Invoice;
use Site\CoreDomain\Invoice\Product;
use Site\CoreDomain\Invoice\ProductRepository;
use Doctrine\ORM\EntityRepository;

/**
 * Class ProductDoctrineRepository
 * @package Site\CoreDomainBundle\Repository
 */
class ProductDoctrineRepository extends EntityRepository implements ProductRepository
{
    public function findByInvoice(Invoice $invoice)
    {
        return $this->getEntityManager()->createQuery('
            SELECT p FROM SiteCoreDomainBundle:Product p WHERE p.invoice = :invoiceId ORDER BY p.sort ASC
        ')
            ->setParameter('invoiceId', $invoice->getId())
            ->getResult();
    }

    /**
     * @inheritdoc
     */
    public function add(Product $product)
    {
        $product->setSort($this->getLastSortValue($product->getInvoice()->getId()) + 1);
        $this->_em->persist($product);
        $this->_em->flush();
    }

    /**
     * @inheritdoc
     */
    public function remove(Product $product)
    {
        //$em = $this->getEntityManager();
        $this->_em->remove($product);
        $this->_em->flush();
    }

    /**
     * @inheritdoc
     */
    public function update()
    {
        $this->_em->flush();
    }

    private function getLastSortValue($invoiceId)
    {
        $result =  $this->getEntityManager()->createQuery('
            SELECT MAX(p.sort) sort FROM SiteCoreDomainBundle:Product p WHERE p.invoice = :invoiceId
        ')
            ->setParameter('invoiceId', $invoiceId)
            ->getOneOrNullResult();

        return $result['sort'];
    }
} 