<?php

namespace Site\CoreDomainBundle\Repository;

use Doctrine\DBAL\Connection;

/**
 * Class SupplierRepository
 * @package Site\CoreDomainBundle\Repository
 */
class SupplierRepository
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function findAllActive()
    {
        $suppliers = $this->connection->executeQuery('SELECT id, name FROM postavwiki WHERE is_active = 1 ORDER BY ves ASC')
            ->fetchAll();

        return $suppliers;
    }

    public function find($id)
    {
        $result = $this->connection->executeQuery('SELECT * FROM postavwiki WHERE id = ?',
            array($id),
            array(\PDO::PARAM_INT)
        )->fetch();

        return $result;
    }
} 