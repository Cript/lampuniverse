<?php

namespace Site\CoreDomainBundle\Repository;

use Site\CoreDomain\Invoice\Invoice;
use Site\CoreDomain\Invoice\InvoiceRepository;
use Doctrine\ORM\EntityRepository;

/**
 * Class InvoiceDoctrineRepository
 * @package Site\CoreDomainBundle\Repository
 */
class InvoiceDoctrineRepository extends EntityRepository implements InvoiceRepository
{

    /**
     * @inheritdoc
     */
    public function add(Invoice $invoice)
    {
        $this->_em->persist($invoice);
        $this->_em->flush();
    }

    /**
     * @inheritdoc
     */
    public function remove(Invoice $invoice)
    {
        //$em = $this->getEntityManager();
        $this->_em->remove($invoice);
        $this->_em->flush();
    }

    /**
     * @inheritdoc
     */
    public function update()
    {
        $this->_em->flush();
    }

    /**
     * @inheritdoc
     */
    public function findNewAll()
    {
        return $this->getEntityManager()->createQuery('
            SELECT i FROM SiteCoreDomainBundle:Invoice i WHERE i.status = ?1
        ')
            ->setParameter(1, 'new')
            ->getResult();
    }

    public function findParseAll()
    {
        return $this->getEntityManager()->createQuery('
            SELECT i FROM SiteCoreDomainBundle:Invoice i WHERE i.status = ?1 OR i.status = ?2
        ')
            ->setParameter(1, 'parsed')
            ->setParameter(2, 'processed')
            ->getResult();
    }

    public function findIncome($criteria)
    {
        $qb = $this->createQueryBuilder('i');
        $query = $qb->select('i')
            ->where($qb->expr()->eq('i.status', ':status'))
            ->setParameter('status', 'income', \PDO::PARAM_STR);

        if(!empty($criteria)) {
            foreach ($criteria as $parameterName => $parameterValue) {
                if(empty($parameterValue)) {
                    continue;
                }
                switch ($parameterName) {
                    case 'supplier':
                        $query->andWhere($qb->expr()->eq('i.supplier', ':supplier'));
                        $query->setParameter('supplier', $parameterValue, \PDO::PARAM_INT);
                        break;
                    case 'product_name':
                        $query->leftJoin('i.products', 'p');
                        $query->andWhere($qb->expr()->like('p.name', ':productName'));
                        $query->setParameter('productName', '%'.$parameterValue.'%', \PDO::PARAM_STR);
                        break;
                    case 'date':
                        $dateStart = new \DateTime($parameterValue['date_start']);
                        $dateEnd = new \DateTime($parameterValue['date_end']);

                        if(!empty($parameterValue['date_start'])) {
                            $query->andWhere($qb->expr()->gte('i.date', ':dateStart'));
                            $query->setParameter('dateStart', $dateStart->format("Y-m-d"), \PDO::PARAM_STR);
                        }
                        if(!empty($parameterValue['date_end'])) {
                            $query->andWhere($qb->expr()->lte('i.date', ':dateEnd'));
                            $query->setParameter('dateEnd', $dateEnd->format("Y-m-d"), \PDO::PARAM_STR);
                        }
//                        ld($dateEnd->format("Y-m-d H:i:s"));
//                        echo($query->getQuery()->getDQL());
//                        ldd($parameterValue);
                }
            }

        }

        $result = $query->getQuery()->getResult();

        return $result;

//        $query = $this->getEntityManager()->createQuery('
//            SELECT i FROM SiteCoreDomainBundle:Invoice i WHERE i.status = ?1
//        ')
//            ->setParameter(1, 'income')
//            ->getResult();
    }
} 