<?php

namespace Site\CoreDomainBundle\Services;

use Site\CoreDomain\Invoice\FileHandlerInterface;
use Symfony\Component\Filesystem\Filesystem;

class FileHandlerFilesystem implements FileHandlerInterface
{
    private $filesystem;

    private $path;

//    private $extensionFolder = array(
//        'pdf' => 'pdf',
//        'xls' => 'xls',
//        'xlsx' => 'xls'
//    );

    public function __construct(Filesystem $filesystem, $path)
    {
        $this->filesystem = $filesystem;
        $this->path = $path;
    }

    public function exists($fileName)
    {
        $subPath = $this->guessSubPath($fileName);
        return $this->filesystem->exists($this->path.DIRECTORY_SEPARATOR.$subPath.DIRECTORY_SEPARATOR.$fileName);
    }

    public function save($filePath, $format)
    {
        $path = $this->path.DIRECTORY_SEPARATOR.$format;
        if(false === $this->filesystem->exists($path)) {
            $this->filesystem->mkdir($path, 0774);
        }

        $fileName = uniqid().'.'.$format;

        file_put_contents($path.DIRECTORY_SEPARATOR.$fileName, file_get_contents($filePath));

        return $fileName;
    }

    public function delete($fileName)
    {
        unlink($this->getFullName($fileName));
    }

    public function getFullName($fileName)
    {
        $subPath = $this->guessSubPath($fileName);
        return $this->path.DIRECTORY_SEPARATOR.$subPath.DIRECTORY_SEPARATOR.$fileName;
    }

    public function getWebLink($fileName)
    {
        return sprintf('/files/%s/%s', $this->guessSubPath($fileName), $fileName);
    }

    private function guessSubPath($fileName)
    {
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);
//        if(!isset($this->extensionFolder[$extension])) {
//            throw new \Exception('Configure folder for file type %s in Site\CoreDomainBundle\Services\FileHandlerFilesystem', $extension);
//        }
//        return $this->extensionFolder[$extension];
        return $extension;
    }
} 